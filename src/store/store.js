import { createStore, applyMiddleware } from "redux";
import repoReducer from "./reducers/repoReducer";
import thunk from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(repoReducer, composeWithDevTools(
  applyMiddleware(thunk)
));

export default store;