export class Api {
  constructor(url) {
    this.api = url;
  }

  getData(url) {
    return fetch(`${this.api}${url}`)
      .then(data => data.json())
      .then(res => res);
  }
}
