import { API_URL } from '../consts/index';
import { Api } from './Api';

export const api = new Api(API_URL);
