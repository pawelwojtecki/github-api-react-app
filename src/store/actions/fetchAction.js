import store from "../store";
import { API_URL } from "../../consts";

export const fetch_data = () => {
  return {
    type: "FETCH_REPOS"
  };
};

export const receive_data = data => {
  return {
    type: "FETCHED_REPOS",
    data: data
  };
};

export const receive_error = () => {
  return {
    type: "RECEIVE_ERROR"
  };
};

export const action_repos_fetch = username => {
  store.dispatch(fetch_data());
  return function(dispatch, getState) {
    return fetch(`${API_URL}/users/${username}/repos`)
      .then(data => data.json())
      .then(data => {
        if (data.message === "Not Found") {
          throw new Error("No such repos found!!");
        } else dispatch(receive_data(data));
      })
      .catch(err => dispatch(receive_error()));
  };
};
