import React, { Component } from 'react';

export class User extends Component {

  componentDidMount() {
  }

  render() {
    let { user } = this.props;

    return (
      <div className='user-container'>
        <div>
          <p>USER NAME: {user.login}</p>
          <p>LOCATION: {user.location}</p>
        </div>
        <img alt='PROFILE' src={user.avatar_url} />
      </div>
    );
  }
}
