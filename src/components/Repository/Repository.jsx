import React, { Component } from 'react';

export class Repository extends Component {
  componentDidMount() {
  }

  render() {
    const { repository } = this.props;

    return (
      <div className='flex'>
        <p className='column-2 align-left'>{repository.id}</p>
        <a href={repository.url} className='column-6 align-right'>{repository.name}</a>
      </div>
    );
  }
}
