const initialState = {
  repo_R: {},
  isFetching_R: false,
  isError_R: false
};

const repoReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_REPOS":
      return Object.assign({}, state, {
        isFetching_R: true,
        repo_R: {},
        isError_R: false
      });
    case "FETCHED_REPOS":
      return Object.assign({}, state, {
        repo_R: action.data,
        isFetching_R: false,
        isError_R: false
      });
    case "RECEIVE_ERROR":
      return Object.assign({}, state, {
        isError_R: true,
        isFetching_R: false
      });
    default:
      return state;
  }
};

export default repoReducer;
