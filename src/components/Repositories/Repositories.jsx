import React, { Component } from 'react';
import { Repository } from '../Repository/Repository';

export class Repositories extends Component {

  constructor(props) {
    super(props);

    this.state = {
      visible: 5,
      error: false
    }

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 4 };
    });
  }

  render() {
    const { repositories } = this.props;
    let loader = repositories.slice(0, this.state.visible).map((item, id) => {
      return (
        <div className='repositories-list'>
          <Repository className='repository' repository={item} />
        </div>
      );
    })

    return (
      <div className='repositories-container'>
        <div className='flex'>
          <p className='column-2 align-left'>REPOSITORY ID</p>
          <p className='column-6 align-right'>REPOSITORY NAME</p>
        </div>

        {loader}
        {
          this.state.visible < repositories.length &&
          <button onClick={this.loadMore} type="button" className="load-more">Load more</button>
        }


      </div>
    );
  }
}
