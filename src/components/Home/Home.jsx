import React, { Component } from 'react';
import { api } from '../../services';
import { User } from '../User/User';
import { Repositories } from '../Repositories/Repositories';
import { connect } from "react-redux";
import { action_repos_fetch } from '../../store/actions/fetchAction';

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: 'warsawjs',
      inputValue: '',
      githubUser: [],
      userRepos: [],
      visible: 2,
      error: false
    }
  }

  componentDidMount() {
    // GitHub user info fetch
    api.getData(`/users/${this.state.user}`).then(res => {
      this.setState({ githubUser: res })
    });
    // GitHub user repos fetch
    api.getData(`/users/${this.state.user}/repos`).then(res => {
      this.setState({ userRepos: res })
    });

    this.props.dispatch(action_repos_fetch(this.state.user));
  }

  updateInputValue(evt) {
    this.setState({
      inputValue: evt.target.value
    }, () => this.state.inputValue);
  }

  handleSubmit = e => {
    e.preventDefault();
    const username = this.getUsername.value;
    this.props.dispatch(action_repos_fetch(username));
    this.getUsername.value = "";
  };

  render() {
    let { userRepos } = this.state;
    let repos = !this.props.data_R.repo_R.length ? userRepos : this.props.data_R.repo_R;

    if (!repos.length) {
      return (
        <div className='container'>
          <h2>GITHUB USER REPOSITORIES FETCHER</h2>
          <form onSubmit={this.handleSubmit} className='form-container'>
            <input type="text" value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} ref={input => (this.getUsername = input)} />
            <button>FETCH USER</button>
          </form>
          <p className='align-center'>There is no such user</p>
        </div>
      );
    }

    return (
      <div className='container'>
        <h2>GITHUB USER REPOSITORIES FETCHER</h2>
        <form onSubmit={this.handleSubmit} className='form-container'>
          <input type="text" value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} ref={input => (this.getUsername = input)} />
          <button>FETCH USER</button>
        </form>
        <User user={repos[0].owner} />
        <Repositories repositories={repos} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data_R: state
  };
};

const HomeWithState = connect(mapStateToProps)(Home);
export { HomeWithState as Home };